package com.ingole.umesh.qualitasitassignment.database;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ingole.umesh.qualitasitassignment.model.request.CountryData;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Converters {
    @TypeConverter
    public static List<CountryData> fromString(String value){
        Type listType = new TypeToken<ArrayList<CountryData>>(){}.getType();
        return new Gson().fromJson(value,listType);
    }
    @TypeConverter
    public static String fromArrayList(List<CountryData> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }

}
