package com.ingole.umesh.qualitasitassignment.networks;

import com.google.gson.JsonObject;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("/s/2iodh4vg0eortkl/facts")
    Call<JsonObject> getCanadaDetails();

}
