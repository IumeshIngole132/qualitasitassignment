package com.ingole.umesh.qualitasitassignment.contracter;

import android.content.Context;

public interface MainContracter {
    // it provides commons methods to all fragments/activities
    interface View{
        void initView();
        void setViewData(String data,ApiConstant view);
    }
    // for getData from api and provide it to corresponding classes
    interface Model{
        String getData(String data);
    }
    // for calling api from corresponding classes
    interface Presenter{
        void onClick(ApiConstant caseConstants, String[] parameters, Context context, Boolean showProgressBar);
    }

}
