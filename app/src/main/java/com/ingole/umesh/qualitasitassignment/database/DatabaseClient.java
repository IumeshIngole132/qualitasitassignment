package com.ingole.umesh.qualitasitassignment.database;

import android.arch.persistence.room.Room;
import android.content.Context;

public class DatabaseClient {
    private Context mContext;
    private static DatabaseClient mInClient;
    //our app database object
    private AppDatabase appDatabase;
    private DatabaseClient(Context context){
        this.mContext = context;
        //creating the app database with Room database builder
        //MyToDos is the name of the database
        appDatabase = Room.databaseBuilder(mContext, AppDatabase.class,"CountryDetailsDB")
                .allowMainThreadQueries()
                .build();
    }
    public static synchronized DatabaseClient getInstance(Context mCtx) {
        if (mInClient == null) {
            mInClient = new DatabaseClient(mCtx);
        }
        return mInClient;
    }
    public AppDatabase getAppDatabase() {
        return appDatabase;
    }
}
