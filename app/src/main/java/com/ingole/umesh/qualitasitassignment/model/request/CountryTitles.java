package com.ingole.umesh.qualitasitassignment.model.request;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import java.io.Serializable;
import java.util.List;
@Entity (tableName = "country_titles")
public class CountryTitles implements Serializable {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "rows")
    private List<CountryData> rows = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<CountryData> getRows() {
        return rows;
    }

    public void setRows(List<CountryData> rows) {
        this.rows = rows;
    }
}
