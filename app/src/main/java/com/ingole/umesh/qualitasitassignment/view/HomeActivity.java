package com.ingole.umesh.qualitasitassignment.view;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.ingole.umesh.qualitasitassignment.R;
import com.ingole.umesh.qualitasitassignment.adapters.ShowDetailsAdapter;
import com.ingole.umesh.qualitasitassignment.contracter.ApiConstant;
import com.ingole.umesh.qualitasitassignment.contracter.MainContracter;
import com.ingole.umesh.qualitasitassignment.database.DatabaseClient;
import com.ingole.umesh.qualitasitassignment.model.request.CountryData;
import com.ingole.umesh.qualitasitassignment.model.request.CountryTitles;
import com.ingole.umesh.qualitasitassignment.presenter.MainActivityPresenter;
import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements MainContracter.View{

    MainContracter.Presenter mainPresenter;
    ListView showDetailsLv;
    List<CountryData> countryDataList;
    TextView titleBarText;
    ShowDetailsAdapter showDetailsAdapter;
    Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mainPresenter = new MainActivityPresenter(this);
        mainPresenter.onClick(ApiConstant.GET_CANADA_DETAILS, null,HomeActivity.this,true);
    }

    @Override
    public void initView() {
        //context = this;
        titleBarText = findViewById(R.id.title_txt_view);
        showDetailsLv = findViewById(R.id.show_details_lv);
        final List<CountryData> countryDataList = new ArrayList<>();
        showDetailsAdapter = new ShowDetailsAdapter(countryDataList,HomeActivity.this);
        showDetailsLv.setAdapter(showDetailsAdapter);
        DatabaseClient databaseClient = DatabaseClient.getInstance(HomeActivity.this);
        databaseClient.getAppDatabase().countryDao().getCountryData().observe(HomeActivity.this, new Observer<List<CountryTitles>>() {
            @Override
            public void onChanged(@Nullable List<CountryTitles> countryTitles) {
                if(countryTitles !=null){
                    countryDataList.clear();
                    for(CountryTitles titles : countryTitles){
                        countryDataList.addAll(titles.getRows());
                        titleBarText.setText(titles.getTitle());
                    }
                    showDetailsAdapter.setDataSet(countryDataList);
                    showDetailsAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public void setViewData(String data, ApiConstant view) {
        switch (view) {
            case GET_CANADA_DETAILS:
                CountryTitles countryTitles = new Gson().fromJson(data, CountryTitles.class);
                DatabaseClient databaseClient = DatabaseClient.getInstance(HomeActivity.this);
                databaseClient.getAppDatabase().countryDao().insert(countryTitles);
        }
    }
}
