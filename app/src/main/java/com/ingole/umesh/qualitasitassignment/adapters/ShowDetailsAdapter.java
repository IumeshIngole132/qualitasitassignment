package com.ingole.umesh.qualitasitassignment.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ingole.umesh.qualitasitassignment.R;
import com.ingole.umesh.qualitasitassignment.model.request.CountryData;

import java.util.List;


public class ShowDetailsAdapter extends ArrayAdapter<CountryData> {
    private int lastPosition = -1;
    private List<CountryData> dataSet;
    private Context mContext;
    public ShowDetailsAdapter(List<CountryData> data, Context context) {
        super(context, R.layout.show_details_item, data);
        this.dataSet = data;
        this.mContext=context;
    }

    public void setDataSet(List<CountryData> dataSet) {
        this.dataSet = dataSet;
    }

    @Nullable
    @Override
    public CountryData getItem(int position) {
        return super.getItem(position);
    }

    public static class MyViewHolder  {
        TextView titleTextView;
        TextView descriptionTextView;
        ImageView profileImage;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CountryData dataModel = getItem(position);
        MyViewHolder viewHolder;
        final View result;
        if (convertView == null) {
            viewHolder   = new MyViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.show_details_item, parent, false);

            viewHolder.titleTextView = convertView.findViewById(R.id.title_tex_view);
            viewHolder.descriptionTextView = convertView.findViewById(R.id.discriptions_txt_view);
            viewHolder.profileImage = convertView.findViewById(R.id.profileImage);

            result=convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MyViewHolder)convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        if (dataModel.getTitle() != null) {
            viewHolder.titleTextView.setText(dataModel.getTitle());
        }
        if (dataModel.getDescription() != null) {
            viewHolder.descriptionTextView.setText(dataModel.getDescription());
        }
        if (dataModel != null) {
            Glide.with(mContext).load(dataModel.getImageHref())
                    .placeholder(R.drawable.noimg)
                    .error(R.drawable.noimg)
                    .into(viewHolder.profileImage);
        }
        viewHolder.titleTextView.setTag(position);
        return convertView;
    }

    /* private List<CountryData> countryDataList;
    public void setCountryDataList(List<CountryData> countryDataList) {
        this.countryDataList = countryDataList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.show_details_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if (countryDataList.get(position).getTitle() != null) {
            holder.titleTextView.setText(countryDataList.get(position).getTitle());
        }
        if (countryDataList.get(position).getDescription() != null) {
            holder.descriptionTextView.setText(countryDataList.get(position).getDescription());
        }
        if (countryDataList.get(position) != null) {
            Glide.with(holder.itemView.getContext()).load(countryDataList.get(position).getImageHref())
                    .placeholder(R.drawable.forword_arrow)
                    .error(R.drawable.forword_arrow)
                    .into(holder.profileImage);
        }
    }

    @Override
    public int getItemCount() {
        if (countryDataList == null) return 0;
        return countryDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView titleTextView;
        TextView descriptionTextView;
        ImageView profileImage;

        public MyViewHolder(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.title_tex_view);
            descriptionTextView = itemView.findViewById(R.id.discriptions_txt_view);
            profileImage = itemView.findViewById(R.id.profileImage);

        }
    }*/
}
