package com.ingole.umesh.qualitasitassignment.presenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.WindowManager;

import com.google.gson.JsonObject;
import com.ingole.umesh.qualitasitassignment.contracter.ApiConstant;
import com.ingole.umesh.qualitasitassignment.contracter.MainContracter;
import com.ingole.umesh.qualitasitassignment.model.MainActivityModel;
import com.ingole.umesh.qualitasitassignment.networks.APIClient;
import com.ingole.umesh.qualitasitassignment.networks.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivityPresenter implements MainContracter.Presenter {

    private MainContracter.View mView;
    private MainContracter.Model model;
    ProgressDialog progressDialog;


    public MainActivityPresenter(MainContracter.View view) {
        this.mView = view;
        initPresenter();
    }

    private void initPresenter() {
        model = new MainActivityModel();
        mView.initView(); // calling activity init method.. now user can design its view
    }

    @Override
    public void onClick(ApiConstant caseConstants, String[] parameters, Context context, Boolean showProgressBar) {
        if (showProgressBar) {
            initAndShowProgressBar(context);
        }

        Retrofit retrofit = APIClient.getClient();
        final ApiInterface requirstInterface = retrofit.create(ApiInterface.class);
        Call<JsonObject> asseccesTokenCall;

        switch (caseConstants){
            case GET_CANADA_DETAILS :
                asseccesTokenCall = requirstInterface.getCanadaDetails();
                callApiWithAccessToken(asseccesTokenCall, context,ApiConstant.GET_CANADA_DETAILS);
                break;
        }
    }

    private void initAndShowProgressBar(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMax(100);
        progressDialog.setMessage("Loading...");
        progressDialog.setTitle("Please Wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    private void callApiWithAccessToken(Call<JsonObject> asseccesTokenCall, final Context context, final ApiConstant view) {
        asseccesTokenCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                if (response.body() != null) {
                    String data = model.getData(response.body().toString());
                    mView.setViewData(data, view);
                    Log.d("Response ", "setViewData: "+data);
                } else {
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

}
