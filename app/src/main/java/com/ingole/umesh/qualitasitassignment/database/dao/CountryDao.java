package com.ingole.umesh.qualitasitassignment.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import com.ingole.umesh.qualitasitassignment.model.request.CountryTitles;
import java.util.List;
@Dao
public interface CountryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CountryTitles countryTitles);

    @Query("SELECT * FROM country_titles")
    LiveData<List<CountryTitles>> getCountryData();
}
