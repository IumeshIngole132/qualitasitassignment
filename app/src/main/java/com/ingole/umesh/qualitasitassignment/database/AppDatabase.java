package com.ingole.umesh.qualitasitassignment.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import com.ingole.umesh.qualitasitassignment.database.dao.CountryDao;
import com.ingole.umesh.qualitasitassignment.model.request.CountryData;
import com.ingole.umesh.qualitasitassignment.model.request.CountryTitles;

@Database(entities = {CountryTitles.class, CountryData.class},version = 1,exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {

    public abstract CountryDao countryDao();
}
